﻿using FluentNHibernate.Mapping;

namespace nhib_App_2
{
    class AlbumsMap:ClassMap<Albums>
    {
        public AlbumsMap()
        {
            Table("Albums");
            Id(x => x.AlbumID).GeneratedBy.Native();
            Map(x => x.AlbumName);

            HasManyToMany(x => x.songsInAlbum)
               .Table("SongsInAlbums")
               .ParentKeyColumn("AlbumID")
               .ChildKeyColumn("SongID")
               .Cascade.SaveUpdate()
               .AsBag();

            HasManyToMany(x => x.singersInAlbum)
               .Table("SingersInAlbums")
               .Inverse()
               .ParentKeyColumn("AlbumID")
               .ChildKeyColumn("SingerID")
               .Cascade.SaveUpdate()
               .AsBag();
        }
    }
}
