﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhib_App_2
{
    class Songs
    {
        public virtual int SongID { get; set; }
        public virtual string SongName { get; set; }
        public virtual int Duration { get; set; }
        public virtual IList<Genres> genresInSong { get; set; }
        public virtual IList<Albums> albumsInSong { get; set; }
        public virtual IList<Singers> singersInSong { get; set; }

        public Songs()
        {
            genresInSong = new List<Genres>();
            albumsInSong = new List<Albums>();
            singersInSong = new List<Singers>();
        }

        public Songs(string songname, int duration)
        {
            SongName = songname;
            Duration = duration;
            genresInSong = new List<Genres>();
            albumsInSong = new List<Albums>();
            singersInSong = new List<Singers>();
        }

    }
}
