﻿using FluentNHibernate.Mapping;

namespace nhib_App_2
{
    class SingersMap:ClassMap<Singers>
    {
        public SingersMap()
        {
            Table("Singers");
            Id(x => x.SingerID).GeneratedBy.Native();
            Map(x => x.SingerName);

            HasManyToMany(x => x.songsInSingers)
                .Table("SingersInSongs")
                .ParentKeyColumn("SingerID")
                .ChildKeyColumn("SongID")
                .Cascade.SaveUpdate()
                .AsBag();

            HasManyToMany(x => x.albumsInSingers)
                .Table("SingersInAlbums")
                .ParentKeyColumn("SingerID")
                .ChildKeyColumn("AlbumID")
                .Cascade.SaveUpdate()
                .AsBag();
        }
    }
}
