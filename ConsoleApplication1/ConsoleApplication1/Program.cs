﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Criterion;
namespace nhib_App_2
{
    class Program
    {

        private static List<SongInfo> info = new List<SongInfo>();
        private static ISessionFactory sessionFactory;
        static void Main(string[] args)
        {
            { }
            NHib_congif();
            ParseFileToList();
            FillDB();
           
                getAlbumOfGroupe("Linkin Park");
                getAllSongFromAllAlbumsOfGroupe("Linkin Park");
                GroupsWithTheSameNameSongs();
                SongWithExtraDuration(180,"Linkin Park");
                SongFromAlbumWithDurationLess3mins("Meteora");
                SongsWith2SpecifiedGenres("Rock", "Pop-Rock");
                SongsWithSpecifiedGere("Rock");
                SongsWithAnyOfSpecifiedGenre("Classic", "Pop-Rock", "Rock-Ballad");
                TheMostPopularGenreInList();
                RatingOfGenres();
                SongsWithUniqeGenre();
                SongsWithoutSpecifiedGenre("Rock");
                CountOfSongsNotInAlbum();
              //  RatingOfGenresInALbums();

        }


        public static void RatingOfGenresInALbums()
        {
            //  Все альбомы с указанием преобладающего жанра (жанров, если количество песен совпадает).
            using (ISession session = sessionFactory.OpenSession())
            {
                Albums albs = null;
                Songs sng = null;
                Genres gn = null;
                IList<Albums> albums = session.QueryOver<Albums>(() => albs)
                    .JoinAlias(() => albs.songsInAlbum, () => sng)
                    .JoinAlias(() => sng.genresInSong, () => gn)
                    .Select(Projections.GroupProperty
                    (Projections.Property<Genres>(x => new { x.GenreName ,albs.AlbumName}))
                    , Projections.Count<Genres>(x => x.GenreName))
                    .List<Albums>();
                    
            }
        }

        public static void CountOfSongsNotInAlbum()
        {
             //  Количество песен, не лежащих в альбомах.
             using (ISession session = sessionFactory.OpenSession())
            {
                IList<string> songs = session.QueryOver<Songs>()
                    .JoinQueryOver<Albums>(x => x.albumsInSong)
                    .Select(x => x.SongName).List<string>();
                List<string> songslist = songs.ToList<string>();
                int ss = session.QueryOver<Songs>()
                    .WhereRestrictionOn(x => x.SongName).Not.IsIn(songslist)
                    .List<Songs>().Count;
                Console.WriteLine(" Количество песен, не лежащих в альбомах: " + ss);
             }
        }

        public static void SongsWithoutSpecifiedGenre(string genre)
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                //  Количество песен, не содержащих указанный жанр.
                Songs ss = null;
                Genres gn = null;
                IList<string> r14 = session.QueryOver<Songs>(() => ss)
                                     .JoinAlias(() => ss.genresInSong, () => gn)
                                     .Where(() => gn.GenreName == genre)
                                     .Select(x => x.SongName).List<string>();
                List<string> r15 =  r14.ToList<string>();
                int songs = 
                     session.QueryOver<Songs>()
                     .WhereRestrictionOn(x=>x.SongName).Not.IsIn(r15)
                    .List<Songs>().Count ;
                Console.WriteLine("Количество песен, не содержащих жанр "+genre+" :"+songs );
            }
        }


        public static void SongsWithUniqeGenre()
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                Genres gn = null;
                Songs ss = null;
                IList<object[]> r13 = session.QueryOver<Genres>(() => gn)
                    .JoinAlias(() => gn.songsInGenres, () => ss)
                    .Select(Projections.GroupProperty
                    (Projections.Property<Genres>(x => x.GenreName))
                    , Projections.Count<Genres>(x => x.GenreName))
                    .Where(Restrictions.Eq(Projections.Count<Genres>(x => x.GenreName), 1))
                    .List<object[]>();
                Console.WriteLine("Uniqe genres : ");
                foreach (var gname in r13) { Console.WriteLine(gname[0]); }

            }
        }

        public static void RatingOfGenres()
        {

            // Рейтинг жанров трэк-листа начиная с самого распространенного.
            using (ISession session = sessionFactory.OpenSession())
            {
                IList<object[]> r10 = (session.QueryOver<Genres>()
                   .JoinQueryOver<Songs>(x => x.songsInGenres))
                   .Select(Projections.ProjectionList()
                   .Add(Projections.Group<Genres>(x => x.GenreName), "GenreName")
                   .Add(Projections.Count<Genres>(x => x.GenreName), "count"))
                   .UnderlyingCriteria.AddOrder(new Order("count", false))
                   .List<object[]>();
                Console.WriteLine("Рейтинг жанров трэк-листа начиная с самого распространенного");
                foreach (var name in r10) { Console.WriteLine(name[0]); }
            }

        }

        public static void TheMostPopularGenreInList()
        {
            //  Жанр, преобладающий во всем трэк-листе.
            using (ISession session = sessionFactory.OpenSession())
            {
                IList<object[]> r9 = (session.QueryOver<Genres>()
                         .JoinQueryOver<Songs>(x => x.songsInGenres))
                         .Select(Projections.GroupProperty
                         (Projections.Property<Genres>(x => x.GenreName))
                         , Projections.Count<Genres>(x => x.GenreName))
                         .OrderBy(Projections.Count<Songs>(x =>x.SongName))
                         .Desc
                         .Take(1)
                         .List<object[]>();
                Console.WriteLine("The most populat genge is " + r9[0][0]);
            }
        }

        public static void SongsWithAnyOfSpecifiedGenre(params string[] genres)
        {

            using (ISession session = sessionFactory.OpenSession())
            {
                //  Все песни, являющиеся одним из указанных жанров.
                IList<Songs> r7 = session.QueryOver<Songs>()
                                    .JoinQueryOver<Genres>(x=>x.genresInSong)
                                    .Where(x=>x.GenreName.IsIn(genres))
                                    .List<Songs>();

                Console.WriteLine("Все песни, являющиеся одним из указанных жанров");
                foreach (Songs sng in r7)
                { Console.WriteLine(sng.SongName); }
            }
        }

        public static void SongsWithSpecifiedGere(string genre)
        {
            // Все песни, указанного жанра.
             using (ISession session = sessionFactory.OpenSession())
            {
                IList<Songs> songs = session.QueryOver<Songs>()
                    .JoinQueryOver<Genres>(x => x.genresInSong)
                    .Where(x => x.GenreName == genre)
                    .List<Songs>();

                Console.WriteLine("Песни жанра " + genre);
                foreach (Songs ss in songs)
                { Console.WriteLine(ss.SongName); }
            }
        }

        public static void SongsWith2SpecifiedGenres(string genre1,string genre2)
        {
            //  Все песни, являющиеся одновременно песнями нескольких указанных жанров.
            using (ISession session = sessionFactory.OpenSession())
            {
                Songs sngs = null;
                Genres gnrs = null;
                IList<object[]> songs = session.QueryOver<Songs>(()=>sngs)
                 .JoinAlias(()=>sngs.genresInSong,()=>gnrs)
                 .Where(()=> gnrs.GenreName == genre1 ||gnrs.GenreName == genre2)
                 .Select(Projections.GroupProperty
                 (Projections.Property<Songs>(x => x.SongName))
                 , Projections.Count<Songs>(x => x.SongName))
                 .Where(Restrictions.Gt(Projections.Count<Songs>(x => x.SongName), 1))
                 .List<object[]>();

                Console.WriteLine("Песни, являющиеся одновременно " +genre1+" и "+genre2);
                foreach (var sng in songs) { Console.WriteLine(sng[0]); }
            }
            
        }

        public static void SongFromAlbumWithDurationLess3mins(string albName)
        {
            //  Все песни из указанного альбома короче 3 минут.
            using (ISession session = sessionFactory.OpenSession())
            {
                IList<Songs> r5 = session.QueryOver<Songs>()
                  .Where(x=>x.Duration<180)
                  .JoinQueryOver<Albums>(c => c.albumsInSong)
                  .Where(x => x.AlbumName == albName)
                  .List<Songs>();

                Console.WriteLine("Все песни из альбома " + albName + " короче 3 минут :");
                foreach (Songs song in r5) { Console.WriteLine(song.SongName); }
            }
        }

        public static void SongWithExtraDuration(int dur,string name)
        {
            // Все песни указанной группы с длительностью, больше указанной.
            using (ISession session = sessionFactory.OpenSession())
            {
                IList<Songs> r4 = session.QueryOver<Songs>()
                          .Where(x=>x.Duration > dur)
                          .JoinQueryOver<Singers>(x=>x.singersInSong)
                          .Where(x=>x.SingerName == name) 
                          .List<Songs>();

                Console.WriteLine("Песни исполнителя " + name + " имеющие длительность более " + dur + " секунд :");
                foreach (Songs song in r4) { Console.WriteLine(song.SongName); }
            }
        }

        public static void GroupsWithTheSameNameSongs()
        {
            // Все пары групп, у которых есть одноименные песни.
            using (ISession session = sessionFactory.OpenSession())
            {
                IList<object[]> doublesongs =
                   session.QueryOver<Songs>()                
                   .Select(Projections.GroupProperty
                   (Projections.Property<Songs>(x => x.SongName))
                   ,Projections.Count<Songs>(x => x.SongName))
                   .Where(Restrictions.Gt(Projections.Count<Songs>(x => x.SongName), 1))
                   .List<object[]>();

                foreach (var spng in doublesongs)
                {
                    IList<Singers> artists = session.QueryOver<Singers>()
                        .JoinQueryOver<Songs>(x => x.songsInSingers)
                        .Where(x => x.SongName == spng[0].ToString())
                        .List<Singers>();

                    Console.WriteLine("Песню под названием " + spng[0] + " имеют следующих два исполнителя :");
                    foreach (Singers sngrs in artists) { Console.WriteLine(sngrs.SingerName); }
                }
                   
            }
        }

        public static void getAllSongFromAllAlbumsOfGroupe(string artisname)
        {
            //Все песни из всех альбомов указанной группы.
            using (ISession session = sessionFactory.OpenSession())
            {
                var w4 = session.QueryOver<Songs>()
                    .JoinQueryOver<Albums>(x => x.albumsInSong)
                    .JoinQueryOver<Singers>(x=>x.singersInAlbum)
                    .Where(x => x.SingerName == "Linkin Park ")                 
                    .List<Songs>();
                Console.WriteLine("Все песни из всех альбомов группы " + artisname + " :");
                foreach (Songs qqq in w4) { Console.WriteLine(qqq.SongName); }
            }
        }

        public static void getAlbumOfGroupe(string artisname)
        {
            // Все альбомы указанной группы.
            using (ISession session = sessionFactory.OpenSession())
            {
                var albums = session.QueryOver<Albums>()
                    .JoinQueryOver<Singers>(x => x.singersInAlbum)
                    .Where(x => x.SingerName == artisname)
                    .List<Albums>();
                Console.WriteLine("Альбомы группы " + artisname + " :");
                foreach (Albums album in albums)  { Console.WriteLine(album.AlbumName); }
            }
        }


        public static void FillDB()
        {
            using (ISession session = sessionFactory.OpenSession())
            {
                foreach (var song in info)
                {
                    using (var transaction = session.BeginTransaction())
                    {
                        Singers singer=null; 
                        Albums album=null;
                        Genres genre = null;
                        Songs sng= null;

                        singer = SingersGetCreate(song.SingerName, session);

                        if (song.AlbumName != null)
                        {
                            album = session.QueryOver<Albums>(()=>album)
                                .JoinAlias(()=>album.singersInAlbum,()=>singer)
                                .Where(() => album.AlbumName == song.AlbumName)   
                                .And(()=>singer.SingerName==song.SingerName)
                                .List<Albums>()
                                .FirstOrDefault();
                            if (album == null) { album = new Albums(song.AlbumName); singer.albumsInSingers.Add(album); }
                        }


                        sng = session.QueryOver<Songs>()                           
                            .Where(x => x.SongName == song.SongName)
                            .JoinQueryOver<Singers>(x=>x.singersInSong)
                            .Where(x=>x.SingerName==song.SingerName)
                            .List<Songs>()
                            .FirstOrDefault();
                        if (sng == null)
                        {
                            sng = new Songs(song.SongName, song.Duration);
                            singer.songsInSingers.Add(sng);
                            if (album != null) { album.songsInAlbum.Add(sng); }
                        }
      

                        foreach (var gen in song.Genres)
                        {
                            genre = session.QueryOver<Genres>()
                                .Where(x => x.GenreName == gen)
                                .List<Genres>()
                                .FirstOrDefault();
                            if (genre == null) { genre = new Genres(gen);  }
                            
                            
                            Genres gn = session.QueryOver<Genres>()                               
                                  .Where(x => x.GenreName == gen)
                                  .JoinQueryOver<Songs>(x=>x.songsInGenres)
                                  .Where(x => x.SongName == song.SongName)
                                  .JoinQueryOver<Singers>(x=>x.singersInSong)
                                  .Where(x=>x.SingerName == song.SingerName)

                                  .List<Genres>()
                                  .FirstOrDefault();
                                 
                            if (gn ==null)
                            sng.genresInSong.Add(genre);
                            session.Save(genre);
                        }
                        session.Save(singer);
                        if (album != null) { session.Save(album); }
                        session.Save(sng);
                        transaction.Commit();
                    }
                }
            }
        }



        public static void NHib_congif()
        {
            FluentConfiguration config = Fluently
                          .Configure()
                          .Database(MsSqlConfiguration
                                        .MsSql2008
                                        .ConnectionString(@"Data Source=антон-пк\sqlexpress;Initial Catalog=NHIB_DB_2;Integrated Security=True;Pooling=False"))
                                        .Mappings(configuration => configuration
                                        .FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()));
            var export = new SchemaUpdate(config.BuildConfiguration());
            export.Execute(true, true);
            sessionFactory = config.BuildSessionFactory();
        }


        public static void ParseFileToList()
        {
            string[] lines = System.IO.File.ReadAllLines(@".\WriteText.txt");
            foreach (string line in lines)
            {
                if (line != "")
                {

                    SongInfo sinfo = new SongInfo();
                    sinfo.AlbumName = null;
                    string[] parts = line.Split(';');
                    string[] time = parts[1].Split(':');
                    sinfo.Duration = Convert.ToInt32(time[0]) * 60 + Convert.ToInt32(time[1]);
                    string[] inform = parts[0].Split('.', '-', '(', ')');
                    sinfo.SongName = inform[1].Trim();
                    sinfo.SingerName = inform[2].Trim();
                    if (inform.Length > 3) { sinfo.AlbumName = inform[3]; }
                    string[] genres = parts[2].Split(',');
                    foreach (var genr in genres) { sinfo.Genres.Add(genr.Trim()); }
                    info.Add(sinfo);
                }
            }
        }

        public static Singers SingersGetCreate(string SingerName, ISession session)
        {
          Singers  singer = session.QueryOver<Singers>()
                            .Where(x => x.SingerName == SingerName)
                            .List<Singers>()
                            .FirstOrDefault();
          if (singer == null) { singer = new Singers(SingerName); }
          return singer;
        }



    }
}
