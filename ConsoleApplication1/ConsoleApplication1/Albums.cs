﻿
using System.Collections.Generic;


namespace nhib_App_2
{
    class Albums
    {
        public virtual int AlbumID { get; set; }
        public virtual string AlbumName { get; set; }
        public virtual IList<Songs> songsInAlbum { get; set; }
        public virtual IList<Singers> singersInAlbum { get; set; }


        public Albums()
        {
            songsInAlbum = new List<Songs>();
            singersInAlbum = new List<Singers>();
        }

        public Albums(string albumname)
        {
            AlbumName = albumname;
            songsInAlbum = new List<Songs>();
            singersInAlbum = new List<Singers>();
        }
    }
}
