﻿using FluentNHibernate.Mapping;

namespace nhib_App_2
{
    class GenresMap:ClassMap<Genres>
    {
        GenresMap()
        {
            Table("Genres");
            Id(x => x.GenreID).GeneratedBy.Native();
            Map(x => x.GenreName);

            HasManyToMany(x =>x.songsInGenres)
              .Table("GenresInSongs")
              .Inverse()
              .ParentKeyColumn("GenreID")
              .ChildKeyColumn("SongID")
              .Cascade.SaveUpdate()
              .AsBag();
        }
    }
}
