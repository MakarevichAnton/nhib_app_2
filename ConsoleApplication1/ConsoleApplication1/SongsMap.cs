﻿using FluentNHibernate.Mapping;

namespace nhib_App_2
{
    class SongsMap:ClassMap<Songs>
    {
        public SongsMap()
        {
            Table("Songs");
            Id(X => X.SongID).GeneratedBy.Native();
            Map(x => x.SongName);
            Map(x => x.Duration);

            HasManyToMany(x =>x.albumsInSong)
              .Table("SongsInAlbums")
              .Inverse()
              .ParentKeyColumn("SongID")
              .ChildKeyColumn("AlbumID")
              .Cascade.SaveUpdate()
              .AsBag();


            HasManyToMany(x => x.singersInSong)
              .Table("SingersInSongs")
              .Inverse()
              .ParentKeyColumn("SongID")
              .ChildKeyColumn("SingerID")
              .Cascade.SaveUpdate()
              .AsBag();


            HasManyToMany(x => x.genresInSong)
              .Table("GenresInSongs")
              .ParentKeyColumn("SongID")
              .ChildKeyColumn("GenreID")
              .Cascade.SaveUpdate()
              .AsBag();

        }
    }
}
