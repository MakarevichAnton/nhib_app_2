﻿using System.Collections.Generic;

namespace nhib_App_2
{
    class Singers
    {
        public virtual int SingerID { get; set; }
        public virtual string SingerName { get; set; }
        public virtual IList<Albums> albumsInSingers { get; set; }
        public virtual IList<Songs> songsInSingers { get; set; }

        public Singers()
        {
            albumsInSingers = new List<Albums>();
            songsInSingers = new List<Songs>();
        }

        public Singers(string singername)
        {
            SingerName = singername;
            albumsInSingers = new List<Albums>();
            songsInSingers = new List<Songs>();
        }
    }
}
