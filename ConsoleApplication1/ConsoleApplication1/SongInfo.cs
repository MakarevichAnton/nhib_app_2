﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhib_App_2
{
    class SongInfo
    {
        public string SingerName { get; set; }
        public string SongName { get; set; }
        public int Duration { get; set; }
        public string AlbumName { get; set; }
        public List<string> Genres { get; set; }

        public SongInfo()
        {
            Genres = new List<string>();
        }

        public SongInfo(string song, string singer, int duratiuon, string album, List<string> genres)
        {
            SongName = song;
            SingerName = singer;
            Duration = duratiuon;
            Genres = genres;

            if (album != null) { AlbumName = album; }
        }
    }
}
