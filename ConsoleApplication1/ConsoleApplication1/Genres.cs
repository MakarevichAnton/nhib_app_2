﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nhib_App_2
{
    class Genres
    {
        public virtual int GenreID { get; set; }
        public virtual string GenreName { get; set; }
        public virtual IList<Songs> songsInGenres { get; set; }

        public Genres()
        {
            songsInGenres = new List<Songs>();
        }

        public Genres(string genrename)
        {
            GenreName = genrename;
            songsInGenres = new List<Songs>();
        }
    }
}
